module FunctionMinimizer

    using LinearAlgebra: norm
    using Random: randperm
    
    export gradientDescent
    export SGD
    export NesterovASGD
    
    "Gradient Descent Function"
    function gradientDescent(   x::Any,
                                y::Any,
                                f_a::Function,
                                df_a::Function,
                                w0::Any,
                                b0::Any;
                                gradLossFunc::Function,
                                mu::Float64=1e-9,
                                maxits::Integer=10000,
                                atol::Float64=1e-16,
                                rtol::Float64=1e-10,
                                normalize::Bool=true,
                                batch_frac::Float64=0.0,
                                verbose::Bool=true )
        w = w0;
        b = b0;
        loss = zeros(maxits);
        iter = -1;
    
        for i in 1:maxits
            tmp = gradLossFunc(x, y, w, b, f_a, df_a, normalize);
            loss[i] = tmp[1];
            dw = tmp[2];
            db = tmp[3];
            iter = i;
            
            if (loss[i] < atol)
                if (verbose)
                    println("gradientDescent: exiting due to loss < atol");
                end
                break;
            end
            if (loss[i]/loss[1] < rtol)
                if (verbose) 
                    println("gradientDescent: exiting due to relative loss < rtol");
                end
                break;
            end
            
            w = w .- mu * dw;
            b = b - mu * db;
            
        end
        
        return (w, b, loss, iter);
        
    end
    
    "Stochastic Gradient Descent Function"
    function SGD( x::Any,
                  y::Any,
                  f_a::Function,
                  df_a::Function,
                  w0::Any,
                  b0::Any;
                  gradLossFunc::Function,
                  mu::Float64=1e-9,
                  maxits::Integer=10000,
                  atol::Float64=1e-16,
                  rtol::Float64=1e-10,
                  stol::Float64=1e-16,
                  normalize::Bool=true,
                  batch_frac::Float64=0.1,
                  verbose::Bool=true )
    
        w = w0;
        b = b0;
        loss = zeros(maxits);
        iter = -1;
    
        n = size(x,2);
        batch_size = convert(Int64, round(batch_frac*n));
        
        for i in 1:maxits
            
            batch_idx = randperm(n);
            batch_idx = batch_idx[1:min(batch_size, n)];
            
            x_batch = x[:, batch_idx];
            if (ndims(y) == 1) 
              y_batch = y[batch_idx];
            else (ndims(y) == 2)
              y_batch = y[:,batch_idx];
            end
            
            tmp = gradLossFunc(x_batch, y_batch, w, b, f_a, df_a, normalize);
            loss[i] = tmp[1];
            dw = tmp[2];
            db = tmp[3];
            iter = i;
            
            if (loss[i] < atol)
                if (verbose)
                    println("stochasticGradientDescent: exiting due to loss < atol");
                end
                break;
            end
            if (loss[i]/loss[1] < rtol)
                if (verbose) 
                    println("stochasticGradientDescent: exiting due to relative loss < rtol");
                end
                break;
            end
            
            w = w .- mu * dw;
            b = b - mu * db;
            
        end
        
        return (w, b, loss, iter);
        
    end
    
    "Nesterov's Accelerated Stochastic Gradient Descent Function"
    function NesterovASGD(  x::Any,
                            y::Any,
                            f_a::Function,
                            df_a::Function,
                            w0::Any,
                            b0::Any;
                            gradLossFunc::Function,
                            mu::Float64=1e-9,
                            maxits::Integer=10000,
                            atol::Float64=1e-16,
                            rtol::Float64=1e-10,
                            stol::Float64=1e-16,
                            normalize::Bool=true,
                            batch_frac::Float64=0.1,
                            verbose::Bool=true )
        w = w0;
        b = b0;
        loss = zeros(maxits);
        iter = -1;
    
        N = size(x,2);
        batch_size = convert(Int64, round(batch_frac*N));
    
        lambda_k = 0;
        q_k = w;
        p_k = b;
    
        for i in 1:maxits
    
            batch_idx = randperm(N);
            batch_idx = batch_idx[1:min(batch_size, N)];
    
            x_batch = x[:, batch_idx];
            if (ndims(y) == 1) 
              y_batch = y[batch_idx];
            else (ndims(y) == 2)
              y_batch = y[:,batch_idx];
            end
    
            tmp = gradLossFunc(x_batch, y_batch, w, b, f_a, df_a, normalize);
            loss[i] = tmp[1];
            dw = tmp[2];
            db = tmp[3];
            iter = i;
    
            if (loss[i] < atol)
                if (verbose)
                    println("NesterovASGD: exiting due to loss < atol");
                end
                break;
            end
            if (loss[i]/loss[1] < rtol)
                if (verbose) 
                    println("NesterovASGD: exiting due to relative loss < rtol");
                end
                break;
            end
    
            q_kp1 = w - mu*dw;
            p_kp1 = b - mu*db;
    
            lambda_kp1 = (1 + sqrt(1 + 4*lambda_k^2)) / 2;
            gamma_k = (1-lambda_k) / lambda_kp1;
    
            w = (1-gamma_k)*q_kp1 + gamma_k*q_k;
            b = (1-gamma_k)*p_kp1 + gamma_k*p_k;
    
            q_k = q_kp1;
            p_k = p_kp1;
            lambda_k = lambda_kp1;
    
        end
    
        return (w, b, loss, iter);
    
    end

end
