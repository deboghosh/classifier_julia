module BinaryClassifier

    using Plots, Printf;
    using Random: seed!, randperm;
    
    export gn
    export gradLoss
    export partitionData;
    export vectorizeData;
    export encodeBinary;
    export binaryDecisionRule;
    export computeAccuracy;
    export classify;
    
    gn( x::AbstractArray, 
        w::AbstractVector, 
        b::Number, 
        f_a::Function) = vec(f_a.(w' * x .+ b));
    
    function gradLoss(  x::AbstractMatrix,
                        y::AbstractVector,
                        w::AbstractVector,
                        b::Number,
                        f_a::Function,
                        df_a::Function,
                        normalize::Bool=true )
    
        loss = 0.0;
        dg_dw = zeros(length(w));
        dg_db = 0.0;
    
        for j in 1:size(x,2)
            error = y[j] - f_a(w'*x[:,j] + b);
            term = error * df_a(w'*x[:,j] + b);
            loss += error^2;
            dg_dw = dg_dw .- (2.0 * term .* x[:,j]);
            dg_db = dg_db - (2.0 * term);
        end
    
        if (normalize)
            loss /= length(y);
            dg_dw /= length(y);
            dg_db /= length(y);
        end
    
        return (loss, dg_dw, dg_db);
    
    end
    
    function partitionData( data::AbstractArray,
                            train_frac::Float64=0.5;
                            split_dim::Integer=ndims(data),
                            seed::Integer=1 )
        N_total = size(data,split_dim);
        N_train = convert(Int64,round(train_frac*N_total));
    
        if (N_train == 0)
            println("partitionData(): warning N_train is 0!");
        end
    
        rand_idx = randperm(seed!(seed), N_total);
        train_idx = rand_idx[1:N_train];
        test_idx = rand_idx[(N_train+1):end];
    
        train_data = selectdim(data, split_dim, train_idx);
        test_data = selectdim(data, split_dim, test_idx);
    
        return train_data, test_data;
    end
    
    function vectorizeData( X::AbstractArray, 
                            slice_dim::Integer=ndims(X) )
        return hcat((vec(slice) for slice in eachslice(X; dims=slice_dim))...)
    end
    
    function encodeBinary(  mat0::AbstractMatrix,
                            mat1::AbstractMatrix,
                            enc_vec::AbstractVector=[0,1])
        N0 = size(mat0,2);
        N1 = size(mat1,2);
        mat = hcat(mat0,mat1);
        tvec = vcat(enc_vec[1]*ones(N0), enc_vec[2]*ones(N1));
        return mat, tvec;
    end
    
    function binaryDecisionRule( x::Float64; encoding_vec::AbstractVector=[0,1] )
        dvec = (x.-encoding_vec).*(x.-encoding_vec);
        return (dvec[1] < dvec[2] ? encoding_vec[1] : encoding_vec[2]);
    end
    
    function computeAccuracy( X::AbstractMatrix, 
                              y::Vector, 
                              w::Vector, 
                              b::Float64, 
                              f_a::Function;
                              neuralNet::Function,
                              encoding_vec::AbstractVector=[0,1] )
        network_output = neuralNet(X, w, b, f_a);
        y_out = binaryDecisionRule.(network_output, encoding_vec=encoding_vec);
        pcorrect = sum(y .== y_out) / length(y);
        return pcorrect, y_out;
    end
    
    function classify(  X1::AbstractArray,
                        name1::String,
                        X2::AbstractArray,
                        name2::String;
                        minimizerFunc::Function,
                        f_a::Function,
                        df_a::Function,
                        gradLoss::Function,
                        gn::Function,
                        learning_rate::Float64=1e-4,
                        train_frac::Float64=0.5,
                        batch_size_frac::Float64=0.1,
                        enc_vec::AbstractVector=[0,1],
                        max_iters::Int64=2000,
                        display_plots::Bool=true,
                        verbose::Bool=true,
                        seed::Integer=1 )
    
        X1_vec = vectorizeData(X1);
        X2_vec = vectorizeData(X2);
    
        X1_train, X1_test = partitionData(X1_vec, train_frac);
        X2_train, X2_test = partitionData(X2_vec, train_frac);
    
        X_train, y_train = encodeBinary(X1_train, X2_train, enc_vec);
        X_test, y_test = encodeBinary(X1_test, X2_test, enc_vec);
    
        if (verbose) 
          @printf("  -->BinaryClassifier.classify(): data vector size = %d\n", size(X1_vec,1));
          @printf("  -->BinaryClassifier.classify().classify(): size of data = %d\n", size(X1_vec,2));
          @printf("  -->BinaryClassifier.classify().classify(): size of training and test data = %d, %d\n", 
                  length(y_train), length(y_test) );
        end

        vec_size = size(X_train,1);
        if seed == false
            w0 = zeros(vec_size);
            b0 = 0.0;
        else
            seed!(seed);
            w = randn(vec_size);
            b = rand();
        end
    
        minimizer_retval = minimizerFunc( X_train,
                                          y_train,
                                          f_a,
                                          df_a,
                                          w0,
                                          b0,
                                          gradLossFunc=gradLoss,
                                          mu=learning_rate,
                                          maxits=max_iters,
                                          batch_frac=batch_size_frac );

        w_hat = minimizer_retval[1];
        b_hat = minimizer_retval[2];
        loss = minimizer_retval[3];
        iters = minimizer_retval[4];

        if (iters < 1)
    
          @printf("  -->BinaryClassifier.classify(): minimizerFunc exited in %d iterations.\n", 
                  iters);
          @printf("  -->BinaryClassifier.classify(): something bad happened.\n");
          
        else
    
          if (verbose)
            @printf("  -->BinaryClassifier.classify(): minimizerFunc exited in %d iterations.\n", 
                    iters);
            @printf("  -->BinaryClassifier.classify(): final loss = %1.4e (abs.), %1.4e (rel.).\n", 
                    loss[iters], loss[iters]/loss[1]);
          end
      
          if (display_plots)
              learning_plot = plot( [1:1:iters], 
                                    loss[1:1:iters], 
                                    yscale=:log10, 
                                    xlabel="iterations", 
                                    ylabel="training loss" );
              plot_fname = "binary_minimizer_convergence_" * name1 * "_" * name2 * ".png";
              savefig(learning_plot, plot_fname)
          end
      
          pcorrect_train, y_pred_train = computeAccuracy( X_train, 
                                                          y_train, 
                                                          w_hat, 
                                                          b_hat, 
                                                          f_a, 
                                                          neuralNet=gn, 
                                                          encoding_vec=enc_vec );
          pcorrect_test, y_pred_test = computeAccuracy( X_test, 
                                                        y_test, 
                                                        w_hat, 
                                                        b_hat, 
                                                        f_a, 
                                                        neuralNet=gn, 
                                                        encoding_vec=enc_vec );
          if (verbose) 
            @printf("  -->BinaryClassifier.classify(): training and test pcorrect = %f, %f.\n", 
                    pcorrect_train, pcorrect_test );
          end
    
        end
    
        return (  w_hat, 
                  b_hat, 
                  pcorrect_test, 
                  pcorrect_train, 
                  X_train, 
                  X_test, 
                  y_train, 
                  y_test, 
                  y_pred_train, 
                  y_pred_test );
    end

end
