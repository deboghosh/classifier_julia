module ActivationFunctions

export linear
export dlinear

export dtanh

export sigmoid
export dsigmoid

# Activation functions
linear(z) = z
dlinear(z) = 1.0

# tanh is already defined in Julia
dtanh(z) = 1 - tanh(z)^2

sigmoid(z::Real) = 1.0 / (1.0 + exp(-z))
function dsigmoid(z::Real)
    sigmoid_z = 1.0 / (1.0 + exp(-z))
    return sigmoid_z * (1 - sigmoid_z)
end

end
