module Classifier

  using Plots, Printf, Flux
  using Random: seed!, randperm
  using Flux: onehotbatch, argmax, crossentropy, throttle, mse, onecold, shuffle
  using Flux.Data: DataLoader
  using IterTools: ncycle, partition
  using CuArrays

  export partitionData
  export vectorizeData

  export encodeDataBinary
  export encodeData

  export decisionRuleBinary
  export decisionRule

  export computeAccuracyBinary
  export computeAccuracy

  export classifyBinary
  export classify

  function partitionData( data::AbstractArray,
                          train_frac::Float64=0.5;
                          split_dim::Integer=ndims(data),
                          seed::Integer=1 )

    N_total = size(data,split_dim)
    N_train = convert(Int64,round(train_frac*N_total))

    if (N_train == 0)
      println("partitionData(): warning N_train is 0!")
    end

    rand_idx = randperm(seed!(seed), N_total)
    train_idx = rand_idx[1:N_train]
    test_idx = rand_idx[(N_train+1):end]

    train_data = selectdim(data, split_dim, train_idx)
    test_data = selectdim(data, split_dim, test_idx)

    return train_data, test_data

  end
    
  function vectorizeData( X::AbstractArray, 
                          slice_dim::Integer=ndims(X) )
    return hcat((vec(slice) for slice in eachslice(X; dims=slice_dim))...)
  end
    
  function encodeDataBinary(mat0::AbstractMatrix,
                            mat1::AbstractMatrix,
                            enc_vec::AbstractVector=[0,1])
    N0 = size(mat0,2)
    N1 = size(mat1,2)
    mat = hcat(mat0,mat1)
    tvec = hcat(enc_vec[1]*ones(1,N0), enc_vec[2]*ones(1,N1))
    return mat, tvec
  end

  function encodeData( Xi::Vector{AbstractMatrix} )
    X = Array{Float64,2} |> gpu
    Y = Array{Float64,2} |> gpu
    n = length(Xi)
    for i in 1:n
      if (i == 1)
        X = Xi[i]
        Y = onehotbatch(i*ones(size(Xi[i],2)), 1:n)
      else
        X = hcat(X, Xi[i]) 
        Y = hcat( Y,  onehotbatch(i*ones(size(Xi[i],2)), 1:n) )
      end
    end
    return X |> gpu, Y |> gpu
  end
    
  function encodeData( Xi::Vector{AbstractArray} )
    n = length(Xi)
    @assert n > 0
    nd = ndims(Xi[1])
    X = Array{Float32,ndims} |> gpu
    Y = Array{Float32,2} |> gpu
    for i in 1:n
      if (i == 1)
        X = Float32.(Xi[i])
        Y = Float32.(onehotbatch(i*ones(size(Xi[i],nd)), 1:n))
      else
        X = cat(X, Float32.(Xi[i]), dims=(nd)) 
        Y = hcat( Y,  Float32.(onehotbatch(i*ones(size(Xi[i],nd)), 1:n)) )
      end
    end
    return X |> gpu, Y |> gpu
  end
    
  function decisionRuleBinary( x::Number; encoding_vec::AbstractVector=[0,1] )
    dvec = (x.-encoding_vec).*(x.-encoding_vec)
    return (dvec[1] < dvec[2] ? encoding_vec[1] : encoding_vec[2])
  end

  function decisionRule(  y::AbstractMatrix, nlabels::Int64 )
    return onecold(y, 1:nlabels)
  end

  function decisionRule(  y::AbstractMatrix, labels::AbstractVector )
    return labels[onecold(y, 1:length(labels))]
  end

  function computeAccuracyBinary( X::AbstractMatrix, 
                                  y::AbstractMatrix, 
                                  model;
                                  encoding_vec::AbstractVector=[0,1] )
      network_output = model(X);
      y_out = decisionRuleBinary.(network_output, encoding_vec=encoding_vec)
      pcorrect = sum(vec(y) .== vec(y_out)) / length(vec(y))
      return pcorrect
  end

  function computeAccuracy( X::AbstractArray,
                            y::AbstractMatrix,
                            model )


    n_labels, n_data = size(y)

    class_indices_actual = decisionRule(y |> cpu, n_labels) |> cpu
    class_indices_predicted = decisionRule(model(X) |> cpu, n_labels) |> cpu

    confusion_matrix = zeros(n_labels, n_labels)
    for i in 1:n_labels
        indices_i = findall(class_indices_actual .== i)
        predictions_i = class_indices_predicted[indices_i]
        for j in 1:n_labels
            confusion_matrix[i, j] = sum(predictions_i .== j) / length(indices_i)
        end
    end

    pcorrect = length(findall(class_indices_actual .== class_indices_predicted)) / length(class_indices_actual)
    return pcorrect, confusion_matrix
  end

"""
Deep Feedforward Networks
"""

  function classifyBinary(  X1::AbstractArray,
                            name1::String,
                            X2::AbstractArray,
                            name2::String;
                            minimizerFunc=ADAM,
                            f_a::AbstractVector=[tanh, tanh],
                            num_neurons::Vector{Int64}=[10, 10],
                            learning_rate::Float64=1e-4,
                            train_frac::Float64=0.5,
                            batch_size_frac::Float64=0.1,
                            enc_vec::AbstractVector=[0,1],
                            lossFunc=mse,
                            epochs::Int64=10,
                            verbose::Bool=true,
                            screen_op_freq::Number=1.0)

    X1_vec = vectorizeData(X1)
    X2_vec = vectorizeData(X2)
    
    X1_train, X1_test = partitionData(X1_vec, train_frac)
    X2_train, X2_test = partitionData(X2_vec, train_frac)
    
    X_train, y_train = encodeDataBinary(X1_train, X2_train, enc_vec)
    X_test, y_test = encodeDataBinary(X1_test, X2_test, enc_vec)

    if (verbose) 
      @printf("  -->Classifier.classifyBinary(): data vector size = %d\n", 
              size(X1_vec,1))
      @printf("  -->Classifier.classifyBinary(): number of samples = %d\n", 
              size(X1_vec,2))
      @printf("  -->Classifier.classifyBinary(): size of training & test data = %d, %d\n",
              length(y_train), length(y_test) )
    end

    n_layers = length(num_neurons);

    model = Chain(Dense(size(X_train,1), num_neurons[1], f_a[1]));
    for i in 1:(n_layers-1)
      model = Chain(model, Dense(num_neurons[i], num_neurons[i+1], f_a[i+1]));
    end
    model = Chain(model, Dense(num_neurons[n_layers], 1));

    loss(x,y) = lossFunc(model(x), y);
    opt = minimizerFunc(learning_rate);
    batch_size = convert(Int64, round(batch_size_frac*size(X_train,2)));
    train_loader = DataLoader(X_train, y_train; batchsize=batch_size, shuffle=true);
    
    initial_loss = loss(X_train, y_train);
    Flux.train!(  loss, 
                  params(model), 
                  ncycle(train_loader, epochs), 
                  opt );
    final_loss = loss(X_train, y_train);

    @printf("  -->Classifier.classify(): final loss = %1.4e (abs.), %1.4e (rel.).\n", 
                final_loss, final_loss/initial_loss )

    y_pred_train = model(X_train);
    y_pred_test = model(X_test);

    pcorrect_train = computeAccuracyBinary( X_train, 
                                            y_train, 
                                            model,
                                            encoding_vec=enc_vec )

    pcorrect_test = computeAccuracyBinary(  X_test, 
                                            y_test, 
                                            model,
                                            encoding_vec=enc_vec )
    if (verbose) 
      @printf("  -->Classifier.classifyBinary(): training and test pcorrect = %f, %f.\n", 
              pcorrect_train, pcorrect_test )
    end

    return (  model, 
              pcorrect_test, 
              pcorrect_train, 
              X_train, 
              X_test, 
              y_train, 
              y_test, 
              y_pred_train, 
              y_pred_test )

  end
    
  function classify(  data::Vector{AbstractArray};
                      minimizerFunc=ADAM,
                      f_a::AbstractVector=[tanh, tanh],
                      num_neurons::Vector{Int64}=[128, 128],
                      learning_rate::Float64=1e-4,
                      train_frac::Float64=0.5,
                      batch_size_frac::Float64=0.1,
                      epochs::Int64=10,
                      lossFunc=crossentropy,
                      verbose::Bool=true,
                      screen_op_freq::Number=1.0)

    n_class = length(data)
    data_train = Vector{AbstractMatrix}(undef, n_class)
    data_test = Vector{AbstractMatrix}(undef, n_class)

    for i in 1:n_class
      data_vec = vectorizeData(data[i])
      data_train[i], data_test[i] = partitionData(data_vec, train_frac)
    end

    X_train, y_train = encodeData(data_train)
    X_test, y_test = encodeData(data_test)

    retval = classify(X_train, y_train,
                      X_test, y_test,
                      minimizerFunc=minimizerFunc,
                      f_a=f_a,
                      num_neurons=num_neurons,
                      learning_rate=learning_rate,
                      batch_size_frac=batch_size_frac,
                      epochs=epochs,
                      lossFunc=lossFunc,
                      verbose=verbose,
                      screen_op_freq=screen_op_freq )


    class_indices_actual_test = decisionRule(y_test |> cpu, n_class)
    class_indices_actual_train = decisionRule(y_train |> cpu, n_class)
    class_indices_pred_test = decisionRule(retval[6], n_class)
    class_indices_pred_train = decisionRule(retval[7], n_class)

    return (  retval[1],  # model
              retval[2],  # pcorrect_test
              retval[3],  # pcorrect_train
              retval[4],  # confusion matrix test
              retval[5],  # confusion matrix train
              X_test |> cpu,
              X_train |> cpu,
              class_indices_actual_test,
              class_indices_actual_train,
              class_indices_pred_test,
              class_indices_pred_train )


  end

  function classify(  x_train_arr::AbstractVector,
                      labels_train::AbstractVector,
                      x_test_arr::AbstractVector,
                      labels_test::AbstractVector;
                      minimizerFunc=ADAM,
                      f_a::AbstractVector=[tanh, tanh],
                      num_neurons::Vector{Int64}=[128, 128],
                      learning_rate::Float64=1e-4,
                      batch_size_frac::Float64=0.1,
                      epochs::Int64=10,
                      lossFunc=crossentropy,
                      verbose::Bool=true,
                      screen_op_freq::Number=1.0)

    data_train = []
    for i in 1:length(x_train_arr)
        data = x_train_arr[i]
        if (i == 1)
            data_train = data
        else
            data_train = cat(data_train, data, dims=(ndims(data)+1))
        end
    end
    data_test = []
    for i in 1:length(x_test_arr)
        data = Float64.(x_test_arr[i])
        if (i == 1)
            data_test = data
        else
            data_test = cat(data_test, data, dims=(ndims(data)+1))
        end
    end

    retval = classify(data_train, labels_train,
                      data_test, labels_test,
                      minimizerFunc=minimizerFunc,
                      f_a=f_a,
                      num_neurons=num_neurons,
                      learning_rate=learning_rate,
                      batch_size_frac=batch_size_frac,
                      epochs=epochs,
                      lossFunc=lossFunc,
                      verbose=verbose,
                      screen_op_freq=screen_op_freq )

    return retval

  end

  function classify(  x_train_arr::AbstractArray,
                      labels_train::AbstractVector,
                      x_test_arr::AbstractArray,
                      labels_test::AbstractVector;
                      minimizerFunc=ADAM,
                      f_a::AbstractVector=[tanh, tanh],
                      num_neurons::Vector{Int64}=[128, 128],
                      learning_rate::Float64=1e-4,
                      batch_size_frac::Float64=0.1,
                      epochs::Int64=10,
                      lossFunc=crossentropy,
                      verbose::Bool=true,
                      screen_op_freq::Number=1.0)

    X_train = vectorizeData(x_train_arr |> gpu) |> gpu
    X_test = vectorizeData(x_test_arr |> gpu) |> gpu

    labels = labels_train |> unique
    num_labels = length(labels)

    y_train = onehotbatch(labels_train |> gpu, labels |> gpu) |> gpu
    y_test = onehotbatch(labels_test |> gpu, labels |> gpu) |> gpu

    retval = classify(X_train, y_train,
                      X_test, y_test,
                      minimizerFunc=minimizerFunc,
                      f_a=f_a,
                      num_neurons=num_neurons,
                      learning_rate=learning_rate,
                      batch_size_frac=batch_size_frac,
                      epochs=epochs,
                      lossFunc=lossFunc,
                      verbose=verbose,
                      screen_op_freq=screen_op_freq )

    y_pred_test = retval[6]
    y_pred_train = retval[7]

    labels_pred_test = decisionRule(y_pred_test |> gpu, labels |> gpu) |> cpu
    labels_pred_train = decisionRule(y_pred_train |> gpu, labels |> gpu) |> cpu

    return (  retval[1],  # model
              retval[2],  # pcorrect_test
              retval[3],  # pcorrect_train
              retval[4],  # confusion matrix test
              retval[5],  # confusion matrix train
              labels_pred_test |> cpu,
              labels_pred_train |> cpu )

  end

  function classify(  X_train::AbstractMatrix,
                      y_train::AbstractMatrix,
                      X_test::AbstractMatrix,
                      y_test::AbstractMatrix;
                      minimizerFunc=ADAM,
                      f_a::AbstractVector=[tanh, tanh],
                      num_neurons::Vector{Int64}=[128, 128],
                      learning_rate::Float64=1e-4,
                      batch_size_frac::Float64=0.1,
                      epochs::Int64=10,
                      lossFunc=crossentropy,
                      verbose::Bool=true,
                      screen_op_freq::Number=1.0)

    n_class = size(y_train,1)

    if (verbose) 
      @printf("  -->Classifier.classify(): number of classes = %d\n", 
              n_class )
      @printf("  -->Classifier.classify(): total number of samples = %d\n", 
              size(X_train,2) + size(X_test,2) )
      @printf("  -->Classifier.classify(): size of training & test data = (%d, %d), (%d, %d)\n",
              size(X_train,1), size(X_train,2), size(X_test,1), size(X_test,2) )
      @printf("  -->Classifier.classify(): size of training & test labels = (%d, %d), (%d, %d)\n",
              size(y_train,1), size(y_train,2), size(y_test,1), size(y_test,2) )
    end

    n_layers = length(num_neurons)
    
    layers = [ Dense(size(X_train,1), num_neurons[1], f_a[1]) ]
    for i in 1:(n_layers-1)
      layers = vcat(layers, Dense(num_neurons[i], num_neurons[i+1], f_a[i+1]) )
    end
    layers = vcat(layers, Dense(num_neurons[n_layers], size(y_train,1)))
    layers = vcat(layers, softmax)
    layers = layers |> gpu
    model(x) = foldl((x,m) -> m(x|>gpu), layers, init=x) |> gpu

    loss(x,y) = lossFunc(model(x), y)
    opt = minimizerFunc(learning_rate)
    batch_size = convert(Int64, round(batch_size_frac*size(X_train,2)))
    train_loader = DataLoader(X_train, y_train; batchsize=batch_size, shuffle=true) |> gpu

    initial_loss = loss(X_train, y_train)
    Flux.train!(  loss, 
                  params(layers), 
                  ncycle(train_loader, epochs), 
                  opt );
    final_loss = loss(X_train, y_train)
    
    @printf("  -->Classifier.classify(): final loss = %1.4e (abs.), %1.4e (rel.).\n", 
                final_loss, final_loss/initial_loss )
      
    y_pred_train = model(X_train) |> gpu
    y_pred_test = model(X_test) |> gpu

    pcorrect_train, confusion_matrix_train = computeAccuracy( X_train, 
                                                              y_train, 
                                                              model )

    pcorrect_test, confusion_matrix_test = computeAccuracy( X_test, 
                                                            y_test, 
                                                            model )

    @printf("  -->Classifier.classify(): training and test pcorrect = %f, %f.\n", 
            pcorrect_train, pcorrect_test )

    return (  model |> cpu,
              pcorrect_test,
              pcorrect_train, 
              confusion_matrix_test,
              confusion_matrix_train,
              y_pred_test |> cpu,
              y_pred_train |> cpu )

  end

"""
Convolutional Networks with Deep Networks
"""

  function convertToTensor(X::AbstractArray)
      @assert  ndims(X) == 3
      new_size = (size(X)...,1)
      return reshape(X, new_size)
  end
    
  function classify(  data::Vector{AbstractArray};
                      minimizerFunc=ADAM,
                      num_channels_cnn::Vector{Int64}=[4,4],
                      k_cnn::Tuple=(),
                      f_a_cnn::AbstractVector=[tanh, tanh],
                      pool_type::String="none",
                      m_cnn::Tuple=(),
                      f_a_dnn::AbstractVector=[tanh, tanh],
                      num_neurons_dnn::Vector{Int64}=[128, 128],
                      learning_rate::Float64=1e-4,
                      train_frac::Float64=0.5,
                      batch_size_frac::Float64=0.1,
                      epochs::Int64=10,
                      lossFunc=crossentropy,
                      verbose::Bool=true,
                      screen_op_freq::Number=1.0)

    n_class = length(data)
    @assert n_class > 0

    data_train = Vector{AbstractArray}(undef, n_class)
    data_test = Vector{AbstractArray}(undef, n_class)

    for i in 1:n_class
      data_train[i], data_test[i] = partitionData(data[i], train_frac)
    end

    X_train, y_train = encodeData(data_train)
    X_test, y_test = encodeData(data_test)

    retval = classify(X_train, y_train,
                      X_test, y_test,
                      minimizerFunc=minimizerFunc,
                      num_channels_cnn=num_channels_cnn,
                      k_cnn=k_cnn,
                      f_a_cnn=f_a_cnn,
                      pool_type=pool_type,
                      m_cnn=m_cnn,
                      f_a_dnn=f_a_dnn,
                      num_neurons_dnn=num_neurons_dnn,
                      learning_rate=learning_rate,
                      batch_size_frac=batch_size_frac,
                      epochs=epochs,
                      lossFunc=lossFunc,
                      verbose=verbose,
                      screen_op_freq=screen_op_freq )


    class_indices_actual_test = decisionRule(y_test |> cpu, n_class)
    class_indices_actual_train = decisionRule(y_train |> cpu, n_class)
    class_indices_pred_test = decisionRule(retval[6], n_class)
    class_indices_pred_train = decisionRule(retval[7], n_class)

    return (  retval[1],  # model
              retval[2],  # pcorrect_test
              retval[3],  # pcorrect_train
              retval[4],  # confusion matrix test
              retval[5],  # confusion matrix train
              X_test |> cpu,
              X_train |> cpu,
              class_indices_actual_test,
              class_indices_actual_train,
              class_indices_pred_test,
              class_indices_pred_train )


  end

  function classify(  x_train_arr::AbstractVector,
                      labels_train::AbstractVector,
                      x_test_arr::AbstractVector,
                      labels_test::AbstractVector;
                      minimizerFunc=ADAM,
                      num_channels_cnn::Vector{Int64}=[4,4],
                      k_cnn::Tuple=(),
                      f_a_cnn::AbstractVector=[tanh, tanh],
                      pool_type::String="none",
                      m_cnn::Tuple=(),
                      f_a_dnn::AbstractVector=[tanh, tanh],
                      num_neurons_dnn::Vector{Int64}=[128, 128],
                      learning_rate::Float64=1e-4,
                      batch_size_frac::Float64=0.1,
                      epochs::Int64=10,
                      lossFunc=crossentropy,
                      verbose::Bool=true,
                      screen_op_freq::Number=1.0)

    data_train = []
    for i in 1:length(x_train_arr)
        data = Float32.(x_train_arr[i])
        if (ndims(data) < 3)
          data = convertToTensor(data)
        end
        if (i == 1)
            data_train = data
        else
            data_train = cat(data_train, data, dims=(ndims(data)+1))
        end
    end
    data_test = []
    for i in 1:length(x_test_arr)
        data = Float32.(x_test_arr[i])
        if (ndims(data) < 3)
          data = convertToTensor(data)
        end
        if (i == 1)
            data_test = data
        else
            data_test = cat(data_test, data, dims=(ndims(data)+1))
        end
    end

    retval = classify(data_train, labels_train,
                      data_test, labels_test,
                      minimizerFunc=minimizerFunc,
                      num_channels_cnn=num_channels_cnn,
                      k_cnn=k_cnn,
                      f_a_cnn=f_a_cnn,
                      pool_type=pool_type,
                      m_cnn=m_cnn,
                      f_a_dnn=f_a_dnn,
                      num_neurons_dnn=num_neurons_dnn,
                      learning_rate=learning_rate,
                      batch_size_frac=batch_size_frac,
                      epochs=epochs,
                      lossFunc=lossFunc,
                      verbose=verbose,
                      screen_op_freq=screen_op_freq )

    return retval

  end

  function classify(  x_train_arr::AbstractArray,
                      labels_train::AbstractVector,
                      x_test_arr::AbstractArray,
                      labels_test::AbstractVector;
                      minimizerFunc=ADAM,
                      num_channels_cnn::Vector{Int64}=[4,4],
                      k_cnn::Tuple=(),
                      f_a_cnn::AbstractVector=[tanh, tanh],
                      pool_type::String="none",
                      m_cnn::Tuple=(),
                      f_a_dnn::AbstractVector=[tanh, tanh],
                      num_neurons_dnn::Vector{Int64}=[128, 128],
                      learning_rate::Float64=1e-4,
                      batch_size_frac::Float64=0.1,
                      epochs::Int64=10,
                      lossFunc=crossentropy,
                      verbose::Bool=true,
                      screen_op_freq::Number=1.0)

    @assert ndims(x_train_arr) > 2
    @assert ndims(x_test_arr) > 2

    if (ndims(x_train_arr) == 3)
      x_train_arr = convertToTensor(x_train_arr)
      x_train_arr = permutedims(x_train_arr, [1,2,4,3])
    end
    if (ndims(x_test_arr) == 3)
      x_test_arr = convertToTensor(x_test_arr)
      x_test_arr = permutedims(x_test_arr, [1,2,4,3])
    end

    X_train = x_train_arr |> gpu
    X_test = x_test_arr |> gpu

    labels = labels_train |> unique
    num_labels = length(labels)

    y_train = onehotbatch(labels_train |> gpu, labels |> gpu) |> gpu
    y_test = onehotbatch(labels_test |> gpu, labels |> gpu) |> gpu

    retval = classify(X_train, y_train,
                      X_test, y_test,
                      minimizerFunc=minimizerFunc,
                      num_channels_cnn=num_channels_cnn,
                      k_cnn=k_cnn,
                      f_a_cnn=f_a_cnn,
                      pool_type=pool_type,
                      m_cnn=m_cnn,
                      f_a_dnn=f_a_dnn,
                      num_neurons_dnn=num_neurons_dnn,
                      learning_rate=learning_rate,
                      batch_size_frac=batch_size_frac,
                      epochs=epochs,
                      lossFunc=lossFunc,
                      verbose=verbose,
                      screen_op_freq=screen_op_freq )

    y_pred_test = retval[6]
    y_pred_train = retval[7]

    labels_pred_test = decisionRule(y_pred_test |> gpu, labels |> gpu) |> cpu
    labels_pred_train = decisionRule(y_pred_train |> gpu, labels |> gpu) |> cpu

    return (  retval[1],  # model
              retval[2],  # pcorrect_test
              retval[3],  # pcorrect_train
              retval[4],  # confusion matrix test
              retval[5],  # confusion matrix train
              labels_pred_test |> cpu,
              labels_pred_train |> cpu )

  end

  function classify(  X_train::AbstractArray,
                      y_train::AbstractMatrix,
                      X_test::AbstractArray,
                      y_test::AbstractMatrix;
                      minimizerFunc=ADAM,
                      num_channels_cnn::Vector{Int64}=[4,4],
                      k_cnn::Tuple=(),
                      f_a_cnn::AbstractVector=[tanh, tanh],
                      pool_type::String="none",
                      m_cnn::Tuple=(),
                      f_a_dnn::AbstractVector=[tanh, tanh],
                      num_neurons_dnn::Vector{Int64}=[128, 128],
                      learning_rate::Float64=1e-4,
                      batch_size_frac::Float64=0.1,
                      epochs::Int64=10,
                      lossFunc=crossentropy,
                      verbose::Bool=true,
                      screen_op_freq::Number=1.0)

    n_class = size(y_train,1)

    if (verbose) 
      @printf("  -->Classifier.classify(): number of classes = %d\n", 
              n_class )
      @printf("  -->Classifier.classify(): total number of samples = %d\n", 
              size(X_train,ndims(X_train)) + size(X_test,ndims(X_test)) )
      println("  -->Classifier.classify(): dimensions of X_train = $(size(X_train))")
      println("  -->Classifier.classify(): dimensions of X_test = $(size(X_test))")
    end

    # convolution network
    n_layers_cnn = length(num_channels_cnn)
    layers_cnn = [  Conv( k_cnn, 
                          size(X_train,ndims(X_train)-1) => num_channels_cnn[1],
                          f_a_cnn[1] ) ]
    if (pool_type == "max")
      layers_cnn = vcat(  layers_cnn,
                          MaxPool( m_cnn ) )
    elseif (pool_type == "mean")
      layers_cnn = vcat(  layers_cnn,
                          MeanPool( m_cnn ) )
    end
    for i in 1:(n_layers_cnn-1)
      layers_cnn = vcat(  layers_cnn,
                          Conv( k_cnn,
                                num_channels_cnn[i] => num_channels_cnn[i+1],
                                f_a_cnn[i+1] ) )
      if (pool_type == "max")
        layers_cnn = vcat(  layers_cnn,
                            MaxPool( m_cnn ) )
      elseif (pool_type == "mean")
        layers_cnn = vcat(  layers_cnn,
                            MeanPool( m_cnn ) )
      end
    end
    layers_cnn = layers_cnn |> gpu
    println("  --> Convolution network layers:-")
    for i in 1:length(layers_cnn)
      println("    $(layers_cnn[i])")
    end
    model_cnn(x) = foldl((x,m) -> m(x|>gpu), layers_cnn, init=x) |> gpu

    # compute conv net output size
    sx = size(X_train)
    sy = ()
    for i in 1:length(sx)-1
      sy = (sy..., 1:sx[i])
    end
    sy = (sy..., [1])
    X_train_sample = X_train[sy...]
    Y_cnn = model_cnn(X_train_sample)
    cnn_out_size = prod(size(Y_cnn))
    println("  -->Classifier.classify(): conv net output dimensions = $(size(Y_cnn))")
    @printf("  -->Classifier.classify(): conv net output size = %d.\n",
            cnn_out_size )

    # deep feedforward network
    n_layers_dnn = length(num_neurons_dnn)
    layers_dnn = [  x -> reshape(x, :, size(x, ndims(x))),
                    Dense(cnn_out_size, num_neurons_dnn[1], f_a_dnn[1]) ]
    for i in 1:(n_layers_dnn-1)
      layers_dnn = vcat(  layers_dnn, 
                          Dense(  num_neurons_dnn[i], 
                                  num_neurons_dnn[i+1], 
                                  f_a_dnn[i+1]) )
    end
    layers_dnn = vcat(  layers_dnn, 
                        Dense(  num_neurons_dnn[n_layers_dnn], 
                                size(y_train,1) ) )
    layers_dnn = vcat(layers_dnn, softmax)
    layers_dnn = layers_dnn |> gpu
    println("  --> Deep feedforward network layers:-")
    for i in 1:length(layers_dnn)
      println("    $(layers_dnn[i])")
    end
    model_dnn(x) = foldl((x,m) -> m(x|>gpu), layers_dnn, init=x) |> gpu

    # join the two networks
    @printf("  -->Classifier.classify(): number of layers in conv net = %d.\n",
            length(layers_cnn) );
    @printf("  -->Classifier.classify(): number of layers in deep net = %d.\n",
            length(layers_dnn) );
    n_layers = length(layers_cnn) + length(layers_dnn)
    layers = Vector{Any}(undef, n_layers)
    for i in 1:length(layers_cnn)
      layers[i] = layers_cnn[i]
    end
    for i in 1:length(layers_dnn)
      layers[i+length(layers_cnn)] = layers_dnn[i]
    end
    layers = layers |> gpu
#    model(x) = foldl((x,m) -> m(x|>gpu), layers, init=x) |> gpu
    model = Chain( layers[1] )
    for i in 2:length(layers)
      model = Chain(model, layers[i])
    end
    model = model |> gpu

    loss(x,y) = lossFunc(model(x), y)
    opt = minimizerFunc(learning_rate)
    batch_size = convert(Int64, round(batch_size_frac*size(X_train,2)))
    train_loader = DataLoader(  X_train, 
                                y_train; 
                                batchsize=batch_size, 
                                shuffle=true) |> gpu

    initial_loss = loss(X_train, y_train)
    Flux.train!(  loss, 
                  params(model), 
                  ncycle(train_loader, epochs), 
                  opt );
    final_loss = loss(X_train, y_train)
    
    @printf("  -->Classifier.classify(): final loss = %1.4e (abs.), %1.4e (rel.).\n", 
                final_loss, final_loss/initial_loss )
      
    y_pred_train = model(X_train) |> gpu
    y_pred_test = model(X_test) |> gpu

    pcorrect_train, confusion_matrix_train = computeAccuracy( X_train, 
                                                              y_train, 
                                                              model )

    pcorrect_test, confusion_matrix_test = computeAccuracy( X_test, 
                                                            y_test, 
                                                            model )

    @printf("  -->Classifier.classify(): training and test pcorrect = %f, %f.\n", 
            pcorrect_train, pcorrect_test )

    return (  model |> cpu,
              pcorrect_test,
              pcorrect_train, 
              confusion_matrix_test,
              confusion_matrix_train,
              y_pred_test |> cpu,
              y_pred_train |> cpu )

  end
    
end
