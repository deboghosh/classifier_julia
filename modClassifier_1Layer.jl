module Classifier

  using Plots, Printf
  using Random: seed!, randperm

  export gBinary
  export g

  export gradLossBinary
  export gradLoss

  export partitionData
  export vectorizeData

  export encodeDataBinary
  export encodeData

  export decisionRuleBinary
  export decisionRule

  export computeAccuracyBinary
  export computeAccuracy

  export classifyBinary
  export classify

  function e_i(i::Integer, n::Integer, T::Type=Float64)
    v = zeros(T, n)
    v[i] = 1
    return v
  end

  function gBinary( X::AbstractMatrix, 
                    W::AbstractMatrix, 
                    b::AbstractVector, 
                    f_a::Function )
    return sum(f_a.(W * X .+ b); dims=1)
  end

  function g( X::AbstractArray, 
              W::AbstractMatrix, 
              b::AbstractVector, 
              f_a::Function )
    return f_a.(W * X .+ b)
  end

  function gradLossBinary(  x::AbstractMatrix,
                            y::AbstractVector,
                            w::AbstractMatrix,
                            b::AbstractVector,
                            f_a::Function,
                            df_a::Function,
                            normalize::Bool=true )

    m, n = size(w)
    N = size(x,2)

    loss = 0.0
    dg_dw = zeros(m, n)
    dg_db = zeros(m)

    for k in 1:N

      term = w*x[:,k] + b
      error = y[k] - sum( f_a.(term) )
      loss += ( (1.0/N) * error^2 )

      for j in 1:m
        deriv_term = error * df_a(term[j])
        for i = 1:n
          dg_dw[j,i] -= ( (2.0/N) * deriv_term * x[i,k] )
        end
        dg_db[j] -= ( (2.0/N) * deriv_term )
      end
    end

    return (loss, dg_dw, dg_db)

  end

  function gradLoss(  x::AbstractMatrix,
                      y::AbstractMatrix,
                      w::AbstractMatrix,
                      b::AbstractVector,
                      f_a::Function,
                      df_a::Function,
                      normalize::Bool=true )

    m, n = size(w)
    N = size(x,2)

    loss = 0.0
    dg_dw = zeros(m, n)
    dg_db = zeros(m)

    for k in 1:N
      for j in 1:m
        term = w[j,:]' * x[:,k] + b[j]
        error = y[j,k] - f_a.(term)
        loss += ( (1.0/N) * error^2 )
        deriv_term = error * df_a.(term)
        for i = 1:n
          dg_dw[j,i] -= ( (2.0/N) * deriv_term * x[i,k] )
        end
        dg_db[j] -= ( (2.0/N) * deriv_term )
      end
    end

    return (loss, dg_dw, dg_db)

  end

  function partitionData( data::AbstractArray,
                          train_frac::Float64=0.5;
                          split_dim::Integer=ndims(data),
                          seed::Integer=1 )

    N_total = size(data,split_dim)
    N_train = convert(Int64,round(train_frac*N_total))

    if (N_train == 0)
      println("partitionData(): warning N_train is 0!")
    end

    rand_idx = randperm(seed!(seed), N_total)
    train_idx = rand_idx[1:N_train]
    test_idx = rand_idx[(N_train+1):end]

    train_data = selectdim(data, split_dim, train_idx)
    test_data = selectdim(data, split_dim, test_idx)

    return train_data, test_data

  end
    
  function vectorizeData( X::AbstractArray, 
                          slice_dim::Integer=ndims(X) )
    return hcat((vec(slice) for slice in eachslice(X; dims=slice_dim))...)
  end
    
  function encodeDataBinary(mat0::AbstractMatrix,
                            mat1::AbstractMatrix,
                            enc_vec::AbstractVector=[0,1])
    N0 = size(mat0,2)
    N1 = size(mat1,2)
    mat = hcat(mat0,mat1)
    tvec = vcat(enc_vec[1]*ones(N0), enc_vec[2]*ones(N1))
    return mat, tvec
  end

  function encodeData( Xi::Vector{AbstractMatrix} )
    X = Array{Float64,2}
    Y = Array{Float64,2}
    n = length(Xi)
    for i in 1:n
      if (i == 1)
        X = Xi[i]
        Y = repeat( e_i(i,n), 1, size(Xi[i],2) )
      else
        X = hcat(X, Xi[i])
        Y = hcat( Y, repeat( e_i(i,n), 1, size(Xi[i],2) ) )
      end
    end
    return X, Y
  end
    
  function decisionRuleBinary( x::Float64; encoding_vec::AbstractVector=[0,1] )
    dvec = (x.-encoding_vec).*(x.-encoding_vec)
    return (dvec[1] < dvec[2] ? encoding_vec[1] : encoding_vec[2])
  end

  function decisionRule(  y::Vector )
    return findmax(y)[2]
  end

  function computeAccuracyBinary( X::AbstractMatrix, 
                                  y::AbstractVector, 
                                  w::AbstractMatrix, 
                                  b::AbstractVector, 
                                  f_a::Function;
                                  gfunc::Function,
                                  encoding_vec::AbstractVector=[0,1] )
      network_output = gfunc(X, w, b, f_a)
      y_out = vec(decisionRuleBinary.(network_output, encoding_vec=encoding_vec))
      pcorrect = sum(y .== y_out) / length(y)
      return pcorrect, y_out
  end

  function computeAccuracy( X::AbstractMatrix,
                            y::AbstractMatrix,
                            w::AbstractMatrix,
                            b::AbstractVector,
                            f_a::Function,
                            gfunc::Function )


    n_labels, n_data = size(y)
    y_out = gfunc(X, w, b, f_a)

    class_indices_actual = Vector(undef, n_data)
    class_indices_predicted = Vector(undef, n_data)
    for i in 1:n_data
      class_indices_actual[i] = decisionRule(y[:,i])
      class_indices_predicted[i] = decisionRule(y_out[:,i])
    end

    confusion_matrix = zeros(n_labels, n_labels)
    for i in 1:n_labels
        indices_i = findall(class_indices_actual .== i)
        predictions_i = class_indices_predicted[indices_i]
        for j in 1:n_labels
            confusion_matrix[i, j] = sum(predictions_i .== j) / length(indices_i)
        end
    end

    pcorrect = length(findall(class_indices_actual .== class_indices_predicted)) / length(class_indices_actual)
    return pcorrect, confusion_matrix, y_out
  end
    
  function classifyBinary(  X1::AbstractArray,
                            name1::String,
                            X2::AbstractArray,
                            name2::String;
                            minimizerFunc::Function,
                            f_a::Function,
                            df_a::Function,
                            gradLoss::Function,
                            gn::Function,
                            num_neurons::Int64=1,
                            learning_rate::Float64=1e-4,
                            train_frac::Float64=0.5,
                            batch_size_frac::Float64=0.1,
                            enc_vec::AbstractVector=[0,1],
                            max_iters::Int64=2000,
                            display_plots::Bool=true,
                            verbose::Bool=true,
                            seed::Integer=1 )

    X1_vec = vectorizeData(X1)
    X2_vec = vectorizeData(X2)
    
    X1_train, X1_test = partitionData(X1_vec, train_frac)
    X2_train, X2_test = partitionData(X2_vec, train_frac)
    
    X_train, y_train = encodeDataBinary(X1_train, X2_train, enc_vec)
    X_test, y_test = encodeDataBinary(X1_test, X2_test, enc_vec)

    if (verbose) 
      @printf("  -->Classifier.classifyBinary(): data vector size = %d\n", 
              size(X1_vec,1))
      @printf("  -->Classifier.classifyBinary(): number of samples = %d\n", 
              size(X1_vec,2))
      @printf("  -->Classifier.classifyBinary(): size of training & test data = %d, %d\n",
              length(y_train), length(y_test) )
    end

    if (seed == false) 
      W0 = zeros(num_neurons, size(X_train,1))
      b0 = zeros(num_neurons)
    else
      seed!(seed)
      W0 = randn(num_neurons, size(X_train,1))
      b0 = randn(num_neurons)
    end

    minimizer_retval = minimizerFunc( X_train,
                                      y_train,
                                      f_a,
                                      df_a,
                                      W0,
                                      b0,
                                      gradLossFunc=gradLoss,
                                      mu=learning_rate,
                                      maxits=max_iters,
                                      batch_frac=batch_size_frac )

    w_hat = minimizer_retval[1]
    b_hat = minimizer_retval[2]
    loss = minimizer_retval[3]
    iters = minimizer_retval[4]

    if (iters < 1)

      @printf("  -->Classifier.classifyBinary(): minimizerFunc exited in %d iterations.\n", 
              iters)
      @printf("  -->Classifier.classifyBinary(): something bad happened.\n")
      
    else

      if (verbose)
        @printf("  -->Classifier.classifyBinary(): minimizerFunc exited in %d iterations.\n", 
                iters)
        @printf("  -->Classifier.classifyBinary(): final loss = %1.4e (abs.), %1.4e (rel.).\n", 
                loss[iters], loss[iters]/loss[1])
      end
      
      if (display_plots)
          learning_plot = plot( [1:1:iters], 
                                loss[1:1:iters], 
                                yscale=:log10, 
                                xlabel="iterations", 
                                ylabel="training loss" )
          plot_fname = "binary_minimizer_convergence_" * name1 * "_" * name2 * ".png";
          savefig(learning_plot,plot_fname)
      end
      
      pcorrect_train, y_pred_train = computeAccuracyBinary( X_train, 
                                                            y_train, 
                                                            w_hat, 
                                                            b_hat, 
                                                            f_a, 
                                                            gfunc=gBinary, 
                                                            encoding_vec=enc_vec )

      pcorrect_test, y_pred_test = computeAccuracyBinary( X_test, 
                                                          y_test, 
                                                          w_hat, 
                                                          b_hat, 
                                                          f_a, 
                                                          gfunc=gBinary, 
                                                          encoding_vec=enc_vec )
      if (verbose) 
        @printf("  -->Classifier.classifyBinary(): training and test pcorrect = %f, %f.\n", 
                pcorrect_train, pcorrect_test )
      end

    end
    
    return (  w_hat, 
              b_hat, 
              pcorrect_test, 
              pcorrect_train, 
              X_train, 
              X_test, 
              y_train, 
              y_test, 
              y_pred_train, 
              y_pred_test )

  end
    
  function classify(  data::Vector{AbstractArray};
                      minimizerFunc::Function,
                      f_a::Function,
                      df_a::Function,
                      gradLoss::Function,
                      gn::Function,
                      learning_rate::Float64=1e-4,
                      train_frac::Float64=0.5,
                      batch_size_frac::Float64=0.1,
                      max_iters::Int64=2000,
                      display_plots::Bool=true,
                      verbose::Bool=true,
                      seed::Integer=1 )

    n_class = length(data)
    data_train = Vector{AbstractMatrix}(undef, n_class)
    data_test = Vector{AbstractMatrix}(undef, n_class)

    for i in 1:n_class
      data_vec = vectorizeData(data[i])
      data_train[i], data_test[i] = partitionData(data_vec, train_frac)
    end

    X_train, y_train = encodeData(data_train)
    X_test, y_test = encodeData(data_test)

    if (verbose) 
      @printf("  -->Classifier.classify(): number of classes = %d\n", 
              n_class )
      @printf("  -->Classifier.classify(): data vector size = %d\n", 
              size(data_train[1],1))
      @printf("  -->Classifier.classify(): total number of samples = %d\n", 
              size(X_train,2) + size(X_test,2) )
      @printf("  -->Classifier.classify(): size of training & test data = (%d, %d), (%d, %d)\n",
              size(X_train,1), size(X_train,2), size(X_test,1), size(X_test,2) )
      @printf("  -->Classifier.classify(): size of training & test labels = (%d, %d), (%d, %d)\n",
              size(y_train,1), size(y_train,2), size(y_test,1), size(y_test,2) )
    end

    if (seed == false) 
      W0 = zeros(n_class, size(X_train,1))
      b0 = zeros(n_class)
    else
      seed!(seed)
      W0 = randn(n_class, size(X_train,1))
      b0 = randn(n_class)
    end

    minimizer_retval = minimizerFunc( X_train,
                                      y_train,
                                      f_a,
                                      df_a,
                                      W0,
                                      b0,
                                      gradLossFunc=gradLoss,
                                      mu=learning_rate,
                                      maxits=max_iters,
                                      batch_frac=batch_size_frac )

    w_hat = minimizer_retval[1]
    b_hat = minimizer_retval[2]
    loss = minimizer_retval[3]
    iters = minimizer_retval[4]

    if (iters < 1)

      @printf("  -->Classifier.classify(): minimizerFunc exited in %d iterations.\n", 
              iters)
      @printf("  -->Classifier.classify(): something bad happened.\n")
      
    else

      if (verbose)
        @printf("  -->Classifier.classify(): minimizerFunc exited in %d iterations.\n", 
                iters)
        @printf("  -->Classifier.classify(): final loss = %1.4e (abs.), %1.4e (rel.).\n", 
                loss[iters], loss[iters]/loss[1])
      end
      
      if (display_plots)
          learning_plot = plot( [1:1:iters], 
                                loss[1:1:iters], 
                                yscale=:log10, 
                                xlabel="iterations", 
                                ylabel="training loss" )
          plot_fname = "minimizer_convergence_" * string(n_class) * "_classes.png";
          savefig(learning_plot,plot_fname)
      end
      
      pcorrect_train, confusion_matrix_train, y_pred_train = computeAccuracy( X_train, 
                                                                              y_train, 
                                                                              w_hat, 
                                                                              b_hat, 
                                                                              f_a, 
                                                                              gn )

      pcorrect_test, confusion_matrix_test, y_pred_test = computeAccuracy(X_test, 
                                                                          y_test, 
                                                                          w_hat, 
                                                                          b_hat, 
                                                                          f_a, 
                                                                          gn )

      if (verbose) 
        @printf("  -->Classifier.classify(): training and test pcorrect = %f, %f.\n", 
                pcorrect_train, pcorrect_test )
      end

    end
    
    return (  w_hat, 
              b_hat, 
              pcorrect_test, 
              pcorrect_train, 
              confusion_matrix_test,
              confusion_matrix_train,
              X_train, 
              X_test, 
              y_train, 
              y_test, 
              y_pred_train, 
              y_pred_test )

  end
    
end
